FROM ubuntu:trusty

# Install java8 with java FX, jdbc driver and other dependencies for soap-ui
RUN \
    echo "===> add webupd8 repository..."  && \
    echo "deb http://ppa.launchpad.net/webupd8team/java/ubuntu trusty main" | tee /etc/apt/sources.list.d/webupd8team-java.list  && \
    echo "deb-src http://ppa.launchpad.net/webupd8team/java/ubuntu trusty main" | tee -a /etc/apt/sources.list.d/webupd8team-java.list  && \
    apt-key adv --keyserver keyserver.ubuntu.com --recv-keys EEA14886 && \
    apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y curl libpostgresql-jdbc-java && \
    echo debconf shared/accepted-oracle-license-v1-1 select true | debconf-set-selections  && \
    echo debconf shared/accepted-oracle-license-v1-1 seen true | debconf-set-selections  && \
    DEBIAN_FRONTEND=noninteractive  apt-get install -y --force-yes oracle-java8-installer oracle-java8-set-default

# Download and install soapui
RUN mkdir -p /opt &&\
    curl  https://s3.amazonaws.com/downloads.eviware/soapuios/5.4.0/SoapUI-5.4.0-linux-bin.tar.gz \
    | tar -xzf - -C /opt

# Copy the jdbc driver jars to the soap-ui ext folder
RUN cp -v /usr/share/java/postgresql-jdbc4*jar /opt/SoapUI-5.4.0/bin/ext/

# Set the entypoint
ENTRYPOINT ["/opt/SoapUI-5.4.0/bin/testrunner.sh"]

# Set default parameters to ENTRYPOINT
CMD ["/test/Petstore-soapui-project.xml", "-s", "generated petstore.swagger.io TestSuite", "-I"]
